﻿using SimpleCore.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;

namespace LFA.Library.Data
{
    /// <summary>
    /// Enable environmental configuration of connection string
    /// </summary>
    public partial class LFAEntities : DbContext
    {

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <returns>database context</returns>
        public static LFAEntities GetConnection()
        {
            return new LFAEntities(Env.GetConnection("db"));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LFAEntities" /> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public LFAEntities(ConnectionStringSettings connection)
            : base(connection.ConnectionString)
        {
        }
    }
}
