﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LFA.Library.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    
    public partial class LFAEntities : DbContext
    {
        public LFAEntities()
            : base("name=LFAEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
    
        public virtual int Lookups_Insert(Nullable<System.Guid> lookupID, string lookupTag, string lookupName, string lookupDescription, string lookupValue, Nullable<short> sortOrder, string lastUpdateBy)
        {
            var lookupIDParameter = lookupID.HasValue ?
                new ObjectParameter("LookupID", lookupID) :
                new ObjectParameter("LookupID", typeof(System.Guid));
    
            var lookupTagParameter = lookupTag != null ?
                new ObjectParameter("LookupTag", lookupTag) :
                new ObjectParameter("LookupTag", typeof(string));
    
            var lookupNameParameter = lookupName != null ?
                new ObjectParameter("LookupName", lookupName) :
                new ObjectParameter("LookupName", typeof(string));
    
            var lookupDescriptionParameter = lookupDescription != null ?
                new ObjectParameter("LookupDescription", lookupDescription) :
                new ObjectParameter("LookupDescription", typeof(string));
    
            var lookupValueParameter = lookupValue != null ?
                new ObjectParameter("LookupValue", lookupValue) :
                new ObjectParameter("LookupValue", typeof(string));
    
            var sortOrderParameter = sortOrder.HasValue ?
                new ObjectParameter("SortOrder", sortOrder) :
                new ObjectParameter("SortOrder", typeof(short));
    
            var lastUpdateByParameter = lastUpdateBy != null ?
                new ObjectParameter("LastUpdateBy", lastUpdateBy) :
                new ObjectParameter("LastUpdateBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Lookups_Insert", lookupIDParameter, lookupTagParameter, lookupNameParameter, lookupDescriptionParameter, lookupValueParameter, sortOrderParameter, lastUpdateByParameter);
        }
    
        public virtual ObjectResult<Lookups_Select_Result> Lookups_Select(string lookupTag, Nullable<System.Guid> lookupID, Nullable<bool> includeDeleted)
        {
            var lookupTagParameter = lookupTag != null ?
                new ObjectParameter("LookupTag", lookupTag) :
                new ObjectParameter("LookupTag", typeof(string));
    
            var lookupIDParameter = lookupID.HasValue ?
                new ObjectParameter("LookupID", lookupID) :
                new ObjectParameter("LookupID", typeof(System.Guid));
    
            var includeDeletedParameter = includeDeleted.HasValue ?
                new ObjectParameter("IncludeDeleted", includeDeleted) :
                new ObjectParameter("IncludeDeleted", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Lookups_Select_Result>("Lookups_Select", lookupTagParameter, lookupIDParameter, includeDeletedParameter);
        }
    
        public virtual ObjectResult<LookupTags_Select_Result> LookupTags_Select()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<LookupTags_Select_Result>("LookupTags_Select");
        }
    
        public virtual int Questions_Insert(Nullable<System.Guid> questionID, string category, string topic, string question, string firstName, string lastName, string email, Nullable<System.Guid> accountID)
        {
            var questionIDParameter = questionID.HasValue ?
                new ObjectParameter("QuestionID", questionID) :
                new ObjectParameter("QuestionID", typeof(System.Guid));
    
            var categoryParameter = category != null ?
                new ObjectParameter("Category", category) :
                new ObjectParameter("Category", typeof(string));
    
            var topicParameter = topic != null ?
                new ObjectParameter("Topic", topic) :
                new ObjectParameter("Topic", typeof(string));
    
            var questionParameter = question != null ?
                new ObjectParameter("Question", question) :
                new ObjectParameter("Question", typeof(string));
    
            var firstNameParameter = firstName != null ?
                new ObjectParameter("FirstName", firstName) :
                new ObjectParameter("FirstName", typeof(string));
    
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Questions_Insert", questionIDParameter, categoryParameter, topicParameter, questionParameter, firstNameParameter, lastNameParameter, emailParameter, accountIDParameter);
        }
    
        public virtual int Accounts_Delete(Nullable<System.Guid> accountID, string changeBy)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            var changeByParameter = changeBy != null ?
                new ObjectParameter("ChangeBy", changeBy) :
                new ObjectParameter("ChangeBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Accounts_Delete", accountIDParameter, changeByParameter);
        }
    
        public virtual int Accounts_Insert(Nullable<System.Guid> accountID, string firstName, string lastName, string email, string password, string changeBy)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            var firstNameParameter = firstName != null ?
                new ObjectParameter("FirstName", firstName) :
                new ObjectParameter("FirstName", typeof(string));
    
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("Password", password) :
                new ObjectParameter("Password", typeof(string));
    
            var changeByParameter = changeBy != null ?
                new ObjectParameter("ChangeBy", changeBy) :
                new ObjectParameter("ChangeBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Accounts_Insert", accountIDParameter, firstNameParameter, lastNameParameter, emailParameter, passwordParameter, changeByParameter);
        }
    
        public virtual ObjectResult<Accounts_Select_Result> Accounts_Select(string firstName, string lastName, string email, Nullable<bool> includeDeleted)
        {
            var firstNameParameter = firstName != null ?
                new ObjectParameter("FirstName", firstName) :
                new ObjectParameter("FirstName", typeof(string));
    
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var includeDeletedParameter = includeDeleted.HasValue ?
                new ObjectParameter("IncludeDeleted", includeDeleted) :
                new ObjectParameter("IncludeDeleted", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Accounts_Select_Result>("Accounts_Select", firstNameParameter, lastNameParameter, emailParameter, includeDeletedParameter);
        }
    
        public virtual ObjectResult<Accounts_Select_Result> Accounts_SelectByAccount(Nullable<System.Guid> accountID, Nullable<bool> includeDeleted)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            var includeDeletedParameter = includeDeleted.HasValue ?
                new ObjectParameter("IncludeDeleted", includeDeleted) :
                new ObjectParameter("IncludeDeleted", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Accounts_Select_Result>("Accounts_SelectByAccount", accountIDParameter, includeDeletedParameter);
        }
    
        public virtual ObjectResult<Accounts_Select_Result> Accounts_SelectByEmail(string email, Nullable<bool> includeDeleted)
        {
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var includeDeletedParameter = includeDeleted.HasValue ?
                new ObjectParameter("IncludeDeleted", includeDeleted) :
                new ObjectParameter("IncludeDeleted", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Accounts_Select_Result>("Accounts_SelectByEmail", emailParameter, includeDeletedParameter);
        }
    
        public virtual int Accounts_Update(Nullable<System.Guid> accountID, string firstName, string lastName, string email, string changeBy)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            var firstNameParameter = firstName != null ?
                new ObjectParameter("FirstName", firstName) :
                new ObjectParameter("FirstName", typeof(string));
    
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var changeByParameter = changeBy != null ?
                new ObjectParameter("ChangeBy", changeBy) :
                new ObjectParameter("ChangeBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Accounts_Update", accountIDParameter, firstNameParameter, lastNameParameter, emailParameter, changeByParameter);
        }
    
        public virtual int Accounts_UpdateLoginFailed(Nullable<System.Guid> accountID, Nullable<int> maxAttempts, string changeBy)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            var maxAttemptsParameter = maxAttempts.HasValue ?
                new ObjectParameter("MaxAttempts", maxAttempts) :
                new ObjectParameter("MaxAttempts", typeof(int));
    
            var changeByParameter = changeBy != null ?
                new ObjectParameter("ChangeBy", changeBy) :
                new ObjectParameter("ChangeBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Accounts_UpdateLoginFailed", accountIDParameter, maxAttemptsParameter, changeByParameter);
        }
    
        public virtual int Accounts_UpdateLoginSuccess(Nullable<System.Guid> accountID, string changeBy)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            var changeByParameter = changeBy != null ?
                new ObjectParameter("ChangeBy", changeBy) :
                new ObjectParameter("ChangeBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Accounts_UpdateLoginSuccess", accountIDParameter, changeByParameter);
        }
    
        public virtual int Accounts_UpdatePassword(Nullable<System.Guid> accountID, string password, string changeBy)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            var passwordParameter = password != null ?
                new ObjectParameter("Password", password) :
                new ObjectParameter("Password", typeof(string));
    
            var changeByParameter = changeBy != null ?
                new ObjectParameter("ChangeBy", changeBy) :
                new ObjectParameter("ChangeBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Accounts_UpdatePassword", accountIDParameter, passwordParameter, changeByParameter);
        }
    
        public virtual int Accounts_UpdateUnlock(Nullable<System.Guid> accountID, string changeBy)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            var changeByParameter = changeBy != null ?
                new ObjectParameter("ChangeBy", changeBy) :
                new ObjectParameter("ChangeBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Accounts_UpdateUnlock", accountIDParameter, changeByParameter);
        }
    
        public virtual int Accounts_UpdateVerified(Nullable<System.Guid> accountID, string changeBy)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            var changeByParameter = changeBy != null ?
                new ObjectParameter("ChangeBy", changeBy) :
                new ObjectParameter("ChangeBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Accounts_UpdateVerified", accountIDParameter, changeByParameter);
        }
    
        public virtual ObjectResult<MenuItems_Select_Result> MenuItems_Select(Nullable<System.Guid> accountID)
        {
            var accountIDParameter = accountID.HasValue ?
                new ObjectParameter("AccountID", accountID) :
                new ObjectParameter("AccountID", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MenuItems_Select_Result>("MenuItems_Select", accountIDParameter);
        }
    }
}
