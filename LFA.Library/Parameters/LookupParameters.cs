﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LFA.Library.Parameters
{
    /// <summary>
    /// Defines list of lookup parameters
    /// </summary>
    public class LookupParameters
    {
        /// <summary>
        /// Gets or sets the lookup ID.
        /// </summary>
        /// <value>The lookup ID.</value>
        public Guid? LookupID { get; set; }

        /// <summary>
        /// Gets or sets the lookup tag.
        /// </summary>
        /// <value>The lookup tag.</value>
        public string LookupTag { get; set; }

        /// <summary>
        /// Gets or sets the include deleted.
        /// </summary>
        /// <value>The include deleted.</value>
        public bool IncludeDeleted { get; set; }
    }
}
