﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LFA.Library.Parameters
{
    /// <summary>
    /// Define the account parameters
    /// </summary>
    public class AccountParameters
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the include deleted.
        /// </summary>
        /// <value>The include deleted.</value>
        public bool IncludeDeleted { get; set; }
    }
}
