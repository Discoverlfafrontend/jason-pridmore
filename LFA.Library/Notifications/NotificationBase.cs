﻿using SimpleCore.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace LFA.Library.Notifications
{
    /// <summary>
    /// Defines a notification base class
    /// </summary>
    public abstract class NotificationBase
        : INotification
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationBase" /> class.
        /// </summary>
        public NotificationBase()
        {
            this.Template = string.Format("notifications/templates/{0}.html", this.GetType().Name);
            this.SenderEmail = Env.Get("email.sender.email");
            this.SenderDisplay = Env.Get("email.sender.display");
            this.Variables = new Dictionary<string, string>();
            this.Variables.Add("AppUrl", Env.Get("env.url"));
        }
        
        /// <summary>
        /// Gets or sets the sender email.
        /// </summary>
        /// <value>The sender email.</value>
        public string SenderEmail { get; set; }

        /// <summary>
        /// Gets or sets the sender display.
        /// </summary>
        /// <value>The sender display.</value>
        public string SenderDisplay { get; set; }

        /// <summary>
        /// Gets or sets the recipients.
        /// </summary>
        /// <value>The recipients.</value>
        public string[] Recipients { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        /// <value>The subject.</value>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the template.
        /// </summary>
        /// <value>The template.</value>
        public string Template { get; set; }

        /// <summary>
        /// Gets or sets the variables.
        /// </summary>
        /// <value>The variables.</value>
        public Dictionary<string, string> Variables { get; set; }

        /// <summary>
        /// Sends the notification.
        /// </summary>
        public void Send()
        {
            if (this.Recipients != null && this.Recipients.Any(r => string.IsNullOrEmpty(r) == false))
            {
                using (var client = new SmtpClient())
                {
                    var msg = new MailMessage();
                    msg.From = new MailAddress(this.SenderEmail, this.SenderDisplay);
                    foreach (var recipient in this.Recipients)
                        msg.To.Add(new MailAddress(recipient));
                    msg.Subject = this.Subject;
                    msg.IsBodyHtml = true;
                    msg.Body = GetBody(this.Template, this.Variables);
                    client.Send(msg);
                }
            }
        }

        /// <summary>
        /// Gets the body.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="variables">The variables.</param>
        /// <returns>body of the template</returns>
        public string GetBody(string template, Dictionary<string, string> variables)
        {
            string body = System.IO.File.ReadAllText(this.Template);
            if (variables != null)
            {
                foreach (var key in variables.Keys)
                {
                    string search = string.Format("%%{0}%%", key);
                    body = body.Replace(search, variables[key]);
                }
                body = System.Text.RegularExpressions.Regex.Replace(body, @"%%[A-Za-z0-9_ ]+%%", "");
            }
            return body;
        }
    }
}
