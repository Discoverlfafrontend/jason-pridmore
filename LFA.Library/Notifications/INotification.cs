﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LFA.Library.Notifications
{
    /// <summary>
    /// Defines a notification
    /// </summary>
    public interface INotification
    {
        /// <summary>
        /// Sends the notification.
        /// </summary>
        void Send();
    }
}
