﻿using LFA.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LFA.Library.Notifications
{
    /// <summary>
    /// Defines an email verification
    /// </summary>
    public class EmailVerification
        : NotificationBase, INotification
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailVerification" /> class.
        /// </summary>
        /// <param name="accountID">The account ID.</param>
        public EmailVerification(AccountModel model)
            : base()
        {
            Configure(model);
        }

        /// <summary>
        /// Configures the specified account ID.
        /// </summary>
        /// <param name="accountID">The account ID.</param>
        public void Configure(AccountModel model)
        {
            this.Subject = "Please verify your LFA account";
            this.Recipients = new string[] { model.Email };
            this.Variables.Add("AccountID", model.AccountID.ToString());
            this.Variables.Add("FirstName", model.FirstName);
            this.Variables.Add("LastName", model.LastName);
        }

    }
}
