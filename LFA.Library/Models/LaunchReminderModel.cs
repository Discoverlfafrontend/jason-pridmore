﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace LFA.Library.Models
{
    /// <summary>
    /// Defines a launch reminder model
    /// </summary>
    public class LaunchReminderModel
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Required!")]
        [StringLength(100, ErrorMessage = "Too long!")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Required!")]
        [StringLength(100, ErrorMessage = "Too long!")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Required!")]
        [StringLength(500, ErrorMessage = "Too long!")]
        public string Email { get; set; }
    }
}
