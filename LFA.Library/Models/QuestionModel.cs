﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace LFA.Library.Models
{
    /// <summary>
    /// Defines the contact question
    /// </summary>
    public class QuestionModel
    {

        /// <summary>
        /// Gets or sets the question ID.
        /// </summary>
        /// <value>The question ID.</value>
        public Guid QuestionID { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>The category.</value>
        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please select a category")]
        [StringLength(500, ErrorMessage = "The category must not be longer than 500 characters")]
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the topic.
        /// </summary>
        /// <value>The topic.</value>
        [Display(Name = "Topic")]
        [Required(ErrorMessage = "Please enter a topic")]
        [StringLength(500, ErrorMessage = "The topic must not be longer than 500 characters")]
        public string Topic { get; set; }

        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        /// <value>The question.</value>
        [Required(ErrorMessage = "Please enter your question")]
        [StringLength(500, ErrorMessage = "The question must not be longer than 500 characters")]
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please provide your first name")]
        [StringLength(100, ErrorMessage = "The first name must not be longer than 100 characters")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please provide your last name")]
        [StringLength(100, ErrorMessage = "The last name must not be longer than 100 characters")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Please provide your email address")]
        [StringLength(500, ErrorMessage = "The email must not be longer than 500 characters")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the account ID.
        /// </summary>
        /// <value>The account ID.</value>
        public Guid? AccountID { get; set; }
    }
}
