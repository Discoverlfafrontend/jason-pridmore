﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace LFA.Library.Models
{
    /// <summary>
    /// Defines the lookup model
    /// </summary>
    public class LookupModel
    {
        /// <summary>
        /// Gets or sets the lookup ID.
        /// </summary>
        /// <value>The lookup ID.</value>
        public Guid LookupID { get; set; }

        /// <summary>
        /// Gets or sets the lookup tag.
        /// </summary>
        /// <value>The lookup tag.</value>
        [Required(ErrorMessage = "The lookup tag is required")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "The lookup tag must be between 2 and 100 characters.")]
        public string LookupTag { get; set; }

        /// <summary>
        /// Gets or sets the name of the lookup.
        /// </summary>
        /// <value>The name of the lookup.</value>
        [Required(ErrorMessage = "The lookup name is required")]
        [StringLength(500, ErrorMessage = "The lookup name may not be longer than 500 characters")]
        public string LookupName { get; set; }

        /// <summary>
        /// Gets or sets the lookup description.
        /// </summary>
        /// <value>The lookup description.</value>
        [StringLength(1000, ErrorMessage = "The lookup description may not be longer than 1,000 characters")]
        public string LookupDescription { get; set; }

        /// <summary>
        /// Gets or sets the lookup value.
        /// </summary>
        /// <value>The lookup value.</value>
        [StringLength(500, ErrorMessage = "The lookup value may not be longer than 500 characters")]
        public string LookupValue { get; set; }

        /// <summary>
        /// Gets or sets the sort order.
        /// </summary>
        /// <value>The sort order.</value>
        [Range(0, 999, ErrorMessage = "The sort order must be between 0 and 999")]
        public short SortOrder { get; set; }

    }
}
