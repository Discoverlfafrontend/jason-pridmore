﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace LFA.Library.Models
{
    /// <summary>
    /// Define an account
    /// </summary>
    public class AccountModel
    {

        /// <summary>
        /// Gets or sets the account ID.
        /// </summary>
        /// <value>The account ID.</value>
        public Guid AccountID { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Required!")]
        [StringLength(100, ErrorMessage = "Too long!")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Required!")]
        [StringLength(100, ErrorMessage = "Too long!")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Required!")]
        [StringLength(500, ErrorMessage = "Too long!")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [Required(ErrorMessage = "Required!")]
        [StringLength(500, ErrorMessage = "Too long!")]
        [MinLength(6, ErrorMessage = "Too short!")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the last login.
        /// </summary>
        /// <value>The last login.</value>
        public DateTime? LastLogin { get; set; }

        /// <summary>
        /// Gets or sets the failed attempts.
        /// </summary>
        /// <value>The failed attempts.</value>
        public int FailedAttempts { get; set; }

        /// <summary>
        /// Gets or sets the is disabled.
        /// </summary>
        /// <value>The is disabled.</value>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// Gets or sets the is locked out.
        /// </summary>
        /// <value>The is locked out.</value>
        public bool IsLockedOut { get; set; }

        /// <summary>
        /// Gets or sets the last lockout.
        /// </summary>
        /// <value>The last lockout.</value>
        public DateTime? LastLockout { get; set; }

        /// <summary>
        /// Gets or sets the is verified.
        /// </summary>
        /// <value>The is verified.</value>
        public bool IsVerified { get; set; }

        /// <summary>
        /// Gets or sets the verification date.
        /// </summary>
        /// <value>The verification date.</value>
        public DateTime? VerificationDate { get; set; }

        /// <summary>
        /// Gets or sets the is deleted.
        /// </summary>
        /// <value>The is deleted.</value>
        public bool IsDeleted { get; set; }

    }
}
