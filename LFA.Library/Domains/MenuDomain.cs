﻿using LFA.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LFA.Library.Domains
{
    /// <summary>
    /// Defines a menu domain
    /// </summary>
    public static class MenuDomain
    {

        /// <summary>
        /// Selects the by account.
        /// </summary>
        /// <param name="accountID">The account ID.</param>
        /// <returns>list of accounts</returns>
        public static MenuItemModel[] SelectByAccount(Guid? accountID)
        {
            using (var db = Data.LFAEntities.GetConnection())
            {
                var items = db.MenuItems_Select(accountID)
                    .Select(o => new MenuItemModel()
                    {
                        ItemID = o.ItemID,
                        Text = o.ItemName,
                        Url = o.Url,
                        Controller = o.ControllerName,
                        Action = o.ActionName,
                        SortOrder = o.SortOrder,
                        ParentID = o.ParentItemID
                    })
                    .ToArray();
                return items;
            }
        }
    }
}
