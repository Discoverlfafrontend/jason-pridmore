﻿using LFA.Library.Models;
using LFA.Library.Notifications;
using LFA.Library.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LFA.Library.Domains
{
    /// <summary>
    /// Define the account domain
    /// </summary>
    public static class AccountDomain
    {

        /// <summary>
        /// Gets the account ID.
        /// </summary>
        /// <returns></returns>
        public static Guid GetAccountID()
        {
            var context = System.Web.HttpContext.Current;
            if (context.User.Identity.IsAuthenticated == false)
                throw new Exception("User not logged in");
            return Guid.Parse(context.User.Identity.Name);
        }

        /// <summary>
        /// Gets the account.
        /// </summary>
        /// <returns>current account</returns>
        public static AccountModel GetAccount()
        {
            return SelectById(GetAccountID());
        }

        /// <summary>
        /// Determines whether [is in role] [the specified role name].
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <returns></returns>
        public static bool IsInRole(string roleName)
        {
            var context = System.Web.HttpContext.Current;
            return context.User.IsInRole(roleName);
        }

        /// <summary>
        /// Gets the account.
        /// </summary>
        /// <param name="accountID">The account ID.</param>
        /// <returns>account model</returns>
        public static AccountModel SelectById(Guid accountID)
        {
            using (var db = Data.LFAEntities.GetConnection())
            {
                var account = db.Accounts_SelectByAccount(accountID, false)
                    .Select(a => Map(a))
                    .FirstOrDefault();
                return account;
            }
        }

        /// <summary>
        /// Gets the account by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>account model</returns>
        public static AccountModel SelectByEmail(string email)
        {
            using (var db = Data.LFAEntities.GetConnection())
            {
                var account = db.Accounts_SelectByEmail(email, false)
                    .Select(a => Map(a))
                    .FirstOrDefault();
                return account;
            }
        }

        /// <summary>
        /// Gets the accounts.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>matching account models</returns>
        public static AccountModel[] Select(AccountParameters parameters)
        {
            using (var db = Data.LFAEntities.GetConnection())
            {
                var accounts = db.Accounts_Select(
                        parameters.FirstName,
                        parameters.LastName,
                        parameters.Email,
                        parameters.IncludeDeleted
                    ).Select(a => Map(a))
                    .ToArray();
                return accounts;
            }
        }

        /// <summary>
        /// Accounts the delete.
        /// </summary>
        /// <param name="accountID">The account ID.</param>
        /// <param name="changeBy">The change by.</param>
        public static void Delete(Guid accountID, string changeBy)
        {
            using (var db = Data.LFAEntities.GetConnection())
            {
                db.Accounts_Delete(accountID, changeBy);
            }
        }

        /// <summary>
        /// Accounts the update.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="changeBy">The change by.</param>
        public static void Update(AccountModel model, string changeBy)
        {
            using (var db = Data.LFAEntities.GetConnection())
            {
                db.Accounts_Update(
                    model.AccountID,
                    model.FirstName,
                    model.LastName,
                    model.Email,
                    changeBy);
            }
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="changeBy">The change by.</param>
        public static void ChangePassword(AccountModel model, string changeBy)
        {
            using (var db = Data.LFAEntities.GetConnection())
            {
                db.Accounts_UpdatePassword(
                    model.AccountID, 
                    HashPassword(model.Password), 
                    changeBy);
            }
        }

        /// <summary>
        /// Accounts the create.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="changeBy">The change by.</param>
        public static void Create(AccountModel model, string changeBy)
        {
            using (var db = Data.LFAEntities.GetConnection())
            {
                if (db.Accounts_SelectByEmail(model.Email, false).Any())
                    throw new Exception("Sorry, but your email address is already registered");
                db.Accounts_Insert(
                    model.AccountID,
                    model.FirstName,
                    model.LastName,
                    model.Email,
                    HashPassword(model.Password),
                    changeBy ?? model.Email);
                SendVerificationEmail(model);
            }
        }

        /// <summary>
        /// Sends the verification email.
        /// </summary>
        /// <param name="email">The email.</param>
        public static void SendVerificationEmail(string email)
        {
            var model = SelectByEmail(email);
            if (model == null)
                throw new Exception("Account not found");
            SendVerificationEmail(model);
        }

        /// <summary>
        /// Sends the verification email.
        /// </summary>
        /// <param name="model">The model.</param>
        public static void SendVerificationEmail(AccountModel model)
        {
            var notification = new EmailVerification(model);
            notification.Send();
        }

        /// <summary>
        /// Hashes the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns>hashed password</returns>
        public static string HashPassword(string password)
        {
            string hashKey = SimpleCore.Config.Env.Get("security.hash.key");
            return SimpleCore.Security.Encryptor.Hash(password, hashKey);
        }

        /// <summary>
        /// Verifies the specified account ID.
        /// </summary>
        /// <param name="accountID">The account ID.</param>
        public static void Verify(Guid accountID)
        {
            var account = SelectById(accountID);
            if (account == null)
                throw new Exception("Invalid verification token");
            if (account.IsVerified == false)
            {
                using (var db = Data.LFAEntities.GetConnection())
                {
                    db.Accounts_UpdateVerified(accountID, account.Email);
                }
            }
        }

        /// <summary>
        /// Accounts the login.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <param name="changeBy">The change by.</param>
        /// <returns>account if validation successful</returns>
        public static AccountModel Login(string email, string password, string changeBy)
        {
            AccountModel account = null;
            try
            {
                account = SelectByEmail(email);
                if (account == null)
                    throw new Exception("Invalid username or password");
                else if (account.IsDeleted)
                    throw new Exception("Account has been deleted");
                else if (account.IsLockedOut
                    && account.LastLockout != null 
                    && DateTime.UtcNow.Subtract(account.LastLockout.Value).TotalHours < 24)
                    throw new Exception("Your account has been locked out for up to 24 hours");
                else if (account.IsDisabled)
                    throw new Exception("Your account has been disabled");
                else if ((account.Password ?? "").Length == 0
                    || account.Password.Equals(HashPassword(password)) == false)
                    throw new Exception("Invalid username or password");
                using (var db = Data.LFAEntities.GetConnection())
                {
                    db.Accounts_UpdateLoginSuccess(account.AccountID, changeBy);
                    System.Web.Security.FormsAuthentication.SetAuthCookie(account.AccountID.ToString(), false);
                    return account;
                }
            }
            catch (Exception ex)
            {
                if (account != null)
                {
                    using (var db = Data.LFAEntities.GetConnection())
                    {
                        int maxAttempts = int.Parse(SimpleCore.Config.Env.Get("security.attempts.max"));
                        db.Accounts_UpdateLoginFailed(account.AccountID, maxAttempts, changeBy);
                    }
                }
                throw ex;
            }
        }

        /// <summary>
        /// Logouts this instance.
        /// </summary>
        public static void Logout()
        {
            try
            {
                System.Web.Security.FormsAuthentication.SignOut();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Maps the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>mapped object</returns>
        public static AccountModel Map(Data.Accounts_Select_Result source)
        {
            var model = new AccountModel()
                {
                    AccountID = source.AccountID,
                    FirstName = source.FirstName,
                    LastName = source.LastName,
                    Email = source.Email,
                    FailedAttempts = source.FailedAttempts,
                    Password = source.Password,
                    LastLogin = source.LastLogin,
                    LastLockout = source.LastLockout,
                    IsLockedOut = source.IsLockedOut,
                    IsDisabled = source.IsDisabled,
                    IsVerified = source.IsVerified,
                    VerificationDate = source.VerificationDate,
                    IsDeleted = source.IsDeleted
                };
            return model;
        }
    }
}
