﻿using System;
using System.Collections.Generic;
using System.Linq;
using LFA.Library.Models;

namespace LFA.Library.Domains
{
    /// <summary>
    /// Defines the support domain
    /// </summary>
    public static class SupportDomain
    {

        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <returns>list of categories</returns>
        public static string[] GetCategories()
        {
            var lookups = LookupDomain.GetLookups(new Parameters.LookupParameters() { LookupTag = "contact.categories" })
                .Select(l => l.LookupValue)
                .ToArray();
            return lookups;
        }

        /// <summary>
        /// Questions the create.
        /// </summary>
        /// <param name="model">The model.</param>
        public static void QuestionCreate(QuestionModel model)
        {
            try
            {
                using (var db = Data.LFAEntities.GetConnection())
                {
                    db.Questions_Insert(
                        model.QuestionID,
                        model.Category,
                        model.Topic,
                        model.Question,
                        model.FirstName,
                        model.LastName,
                        model.Email,
                        model.AccountID);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("The question could not be saved", ex);
            }
        }

    }
}
