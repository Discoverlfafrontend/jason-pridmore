﻿using LFA.Library.Models;
using LFA.Library.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LFA.Library.Domains
{
    /// <summary>
    /// Defines the lookup domain
    /// </summary>
    public static class LookupDomain
    {

        /// <summary>
        /// Gets the lookups.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>arrau pf ;ppli[s</returns>
        public static LookupModel[] GetLookups(LookupParameters parameters)
        {
            using (var db = Data.LFAEntities.GetConnection())
            {
                var records = db.Lookups_Select(parameters.LookupTag, parameters.LookupID, parameters.IncludeDeleted)
                    .Select(l => new LookupModel() {
                        LookupID = l.LookupID,
                        LookupTag = l.LookupTag,
                        LookupName = l.LookupName,
                        LookupDescription = l.LookupDescription,
                        LookupValue = l.LookupValue,
                        SortOrder = l.SortOrder
                    }).ToArray();
                return records;
            }
        }

    }
}
