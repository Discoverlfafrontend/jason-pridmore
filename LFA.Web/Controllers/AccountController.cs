﻿using LFA.Library.Domains;
using LFA.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LFA.Web.Controllers
{
    public class AccountController : Controller
    {

        // GET: /Account/Register
        public ActionResult Register()
        {
            var model = new AccountModel();
            model.AccountID = Guid.NewGuid();
            return View(model);
        }

        // POST: /Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(AccountModel model, string passwordConfirm)
        {
            try
            {
                // Save the record
                if ((model.Password ?? "").Equals(passwordConfirm) == false)
                    ModelState.AddModelError("passwordConfirm", "Does not match");
                if (ModelState.IsValid == false)
                    throw new Exception("There is an issue with one or more fields");
                AccountDomain.Create(model, null);
                return View("JoinThankYou");
            }
            catch (Exception ex)
            {
                // Show the error
                ViewBag.ErrorMessage = ex.Message;
                return View(model);
            }
        }

        // GET: /Account/Verify/id
        public ActionResult Verify(Guid? id)
        {
            try
            {
                if (id.HasValue == false)
                    throw new Exception("Invalid verification token");
                AccountDomain.Verify(id.Value);
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("VerifyError");
            }
        }

        // GET: /Account/Login
        public ActionResult Login()
        {
            var model = new AccountModel();
            model.AccountID = Guid.NewGuid();
            return View(model);
        }

        // POST: /Account/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(AccountModel model)
        {
            try
            {

                // Save the record
                AccountDomain.Login(model.Email, model.Password, model.Email);
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                // Show the error
                ViewBag.ErrorMessage = ex.Message;
                return View(model);
            }
        }

        // GET: /Account/Logout
        public ActionResult Logout()
        {
            AccountDomain.Logout();
            return RedirectToAction("Index", "Home");
        }

        // GET: /Account/Update
        [Authorize]
        public ActionResult Update()
        {
            var model = AccountDomain.GetAccount();
            model.Password = "hidden";
            return View(model);
        }

        // POST: /Account/Update
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Update(AccountModel model)
        {
            try
            {
                // Save the record
                if (ModelState.IsValid == false)
                    throw new Exception("There is an issue with one or more fields");
                AccountDomain.Update(model, model.Email);
                ViewBag.SuccessMessage = "Your profile was updated successfully";
                return View(model);
            }
            catch (Exception ex)
            {
                // Show the error
                ViewBag.ErrorMessage = ex.Message;
                return View(model);
            }
        }

        // GET: /Account/ChangePassword
        [Authorize]
        public ActionResult ChangePassword()
        {
            var model = AccountDomain.GetAccount();
            model.Password = "";
            return View(model);
        }

        // POST: /Account/ChangePassword
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(AccountModel model, string passwordConfirm)
        {
            try
            {
                // Save the record
                if ((model.Password ?? "").Equals(passwordConfirm) == false)
                    ModelState.AddModelError("passwordConfirm", "Does not match");
                if (ModelState.IsValid == false)
                    throw new Exception("There is an issue with one or more fields");
                AccountDomain.ChangePassword(model, model.Email);
                ViewBag.SuccessMessage = "Your profile was updated successfully";
                return View(model);
            }
            catch (Exception ex)
            {
                // Show the error
                ViewBag.ErrorMessage = ex.Message;
                return View(model);
            }
        }

    }
}
