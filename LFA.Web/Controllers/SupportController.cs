﻿using LFA.Library.Domains;
using LFA.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LFA.Web.Controllers
{
    public class SupportController : Controller
    {

        public ActionResult Index()
        {
            return RedirectToAction("Contact");
        }

        public ActionResult Contact()
        {
            ViewBag.Categories = SupportDomain.GetCategories()
                .Select(c => new SelectListItem()
                {
                    Text = c,
                    Value = c
                });
            var model = new QuestionModel();
            model.QuestionID = Guid.NewGuid();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(QuestionModel model)
        {
            try
            {
                // Save the record
                if (ModelState.IsValid == false)
                    throw new Exception("There is an issue with one or more fields");
                SupportDomain.QuestionCreate(model);
                return View("ContactThankYou");
            }
            catch (Exception ex)
            {
                // Show the error
                ViewBag.Categories = SupportDomain.GetCategories()
                    .Select(c => new SelectListItem()
                    {
                        Text = c,
                        Value = c
                    });
                ViewBag.ErrorMessage = ex.Message;
                return View(model);
            }
        }

        public ActionResult Faqs()
        {
            return View();
        }

    }
}
