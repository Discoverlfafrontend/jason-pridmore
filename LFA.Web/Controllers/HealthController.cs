﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LFA.Web.Controllers
{
    public class HealthController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Goals()
        {
            return View();
        }

        public ActionResult Offers()
        {
            return View();
        }

        public ActionResult Trainers()
        {
            return View();
        }

        public ActionResult Tips()
        {
            return View();
        }

    }
}
