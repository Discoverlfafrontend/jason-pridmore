﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LFA.Web.Controllers
{
    public class AboutController : Controller
    {

        public ActionResult Who()
        {
            return View();
        }

        public ActionResult What()
        {
            return View();
        }

        public ActionResult Why()
        {
            return View();
        }

        public ActionResult Foundation()
        {
            return View();
        }

    }
}
