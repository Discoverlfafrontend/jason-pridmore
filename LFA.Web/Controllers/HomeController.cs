﻿using LFA.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LFA.Web.Controllers
{
    /// <summary>
    /// Defines the home controller
    /// </summary>
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        // GET: /Home/Unavailable
        public ActionResult Unavailable()
        {
            return View(new LaunchReminderModel());
        }

        // POST: /Home/Unavailable
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Unavailable(LaunchReminderModel model)
        {
            try
            {
                // Save the record
                if (ModelState.IsValid == false)
                    throw new Exception("There is an issue with one or more fields");
                using (var db = new System.Data.SqlClient.SqlConnection("data source=htpc;initial catalog=LFA_Signups;persist security info=True;user id=LFAUser;password=ojs8@09aaKJ!;MultipleActiveResultSets=True;"))
                {
                    db.Open();
                    using (var cmd = new System.Data.SqlClient.SqlCommand("Signups_Insert", db))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Email", model.Email);
                        cmd.Parameters.AddWithValue("@FirstName", model.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", model.LastName);
                        cmd.ExecuteNonQuery();
                    }
                }
                return View("UnavailableSignup");
            }
            catch (Exception ex)
            {
                // Show the error
                ViewBag.ErrorMessage = ex.Message;
                return View(model);
            }
        }

        public ActionResult Privacy()
        {
            return View();
        }

        public ActionResult Terms()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

    }
}
