﻿using LFA.Library.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LFA.Web.Attributes
{
    /// <summary>
    /// Define an attribute to load the menu
    /// </summary>
    public class HasMenuAttribute
        : ActionFilterAttribute
    {

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action result executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Guid? accountID = null;
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
                accountID = Guid.Parse(filterContext.HttpContext.User.Identity.Name);
            filterContext.Controller.ViewBag.Menu = MenuDomain.SelectByAccount(accountID);
        }

    }
}