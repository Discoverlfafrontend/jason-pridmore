﻿using SimpleCore.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LFA.Web.Attributes
{
    /// <summary>
    /// Defines the unavailable filter
    /// </summary>
    public class UnavailableAttribute
        : ActionFilterAttribute
    {
        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Env.Get("env.live").ToLower().Equals("true") == false)
            {
                if (filterContext.ActionDescriptor.ActionName.ToLower().Equals("unavailable") == false
                    || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower().Equals("home") == false)
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary 
                            {
                                { "action", "Unavailable" },
                                { "controller", "Home" }
                            });
                }
            }
        }
    }
}