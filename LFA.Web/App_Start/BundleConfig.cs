﻿using Microsoft.Web.Optimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LFA.Web.App_Start
{
    /// <summary>
    /// Manages bundle configuration
    /// </summary>
    public static class BundleConfig
    {
        /// <summary>
        /// Registers the bundles.
        /// </summary>
        public static void RegisterBundles()
        {

            // Build script bundle
            var jsBundle = new Bundle("~/bundles/js/plugins", typeof(JsMinify));
            jsBundle.AddDirectory("~/Content/Scripts/Plugins", "*.js", true);
            BundleTable.Bundles.Add(jsBundle);

            // Build style bundle
            var cssBundle = new Bundle("~/bundles/css/common", typeof(CssMinify));
            cssBundle.AddDirectory("~/Content/Styles/Common", "*.css", true);
            BundleTable.Bundles.Add(cssBundle);

        }
    }
}