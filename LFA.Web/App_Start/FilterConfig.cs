﻿using LFA.Web.Attributes;
using System.Web;
using System.Web.Mvc;

namespace LFA.Web.App_Start
{
    /// <summary>
    /// Defines the filter configuration
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Registers the global filters.
        /// </summary>
        /// <param name="filters">The filters.</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new UnavailableAttribute());
            filters.Add(new HasMenuAttribute());
        }
    }
}