﻿// Add site-wide scripts and events
(function () {

    $(document).ready(function () {

        // Scroll to top
        $("a[href='#top']").click(function () {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        });

        // Disable autocomplete
        $('form').attr('autocomplete', 'off');

        // Focus on element
        $('.focus').focus();


    });
}());