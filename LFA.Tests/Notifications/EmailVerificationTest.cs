﻿using LFA.Library.Domains;
using LFA.Library.Notifications;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LFA.Tests.Notifications
{
    [TestClass]
    public class EmailVerificationTest
    {
        [TestMethod]
        public void SendTest()
        {
            var account = AccountDomain.SelectByEmail("wallace.jason@gmail.com");
            Assert.IsNotNull(account);
            var target = new EmailVerification(account);
            target.Send();
        }
    }
}
